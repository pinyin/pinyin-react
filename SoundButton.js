'use strict';

var React = require('react-native');
var AudioPlayer = require('react-native-audioplayer');

var {
  StyleSheet,
  Component,
  View,
  Text,
  TouchableOpacity,
} = React;

class SoundButton extends Component {
    constructor(props) {
        super(props);
        this.bindMethods();
    }

    bindMethods() {
        if (!this.bindableMethods) {
            return;
        }

        for (var methodName in this.bindableMethods) {
            this[methodName] = this.bindableMethods[methodName].bind(this);
        }
    }

    componentDidMount() {
    }

    render() {

        let tone = this.props.tone;

        if(!tone.label) return (
            <TouchableOpacity style={styles.container}></TouchableOpacity>
        )

        return (
            <TouchableOpacity onPress={() => this.onPressButton(tone.tone)} style={styles.container}>
                <View style={styles.circle} >
                    <Text style={styles.rowText}>{tone.label}</Text>
                </View>
            </TouchableOpacity>
        )

    }

    onPressButton(tone) {

        AudioPlayer.play(tone + '.mp3');

    }

}


var styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center'
    },
    circle: {
        height: 70,
        borderRadius: 35,
        width: 70,
        backgroundColor: '#FFFFFF',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    text: {
        color: 'white',
        paddingHorizontal: 8,
        fontSize: 16
    },
    rowStyle: {
        paddingVertical: 16,
        paddingLeft: 16,
        borderTopColor: 'white',
        borderLeftColor: 'white',
        borderRightColor: 'white',
        borderBottomColor: '#E0E0E0',
        borderWidth: 1
    },
    rowText: {
        color: '#212121',
        fontSize: 16
    }
});

module.exports = SoundButton;
