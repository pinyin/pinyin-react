'use strict';

var React = require('react-native');
var {
    StyleSheet,
    Component,
    View,
    Text,
    Navigator,
    TouchableOpacity,
    } = React;

class NavigationBar extends Component {
    render() {
        return (
            <Navigator.NavigationBar routeMapper={NavigationBarRouteMapper} />
        );
    }
}

var NavigationBarRouteMapper = {

    Next(navigator) {

        NEXT_ROUTE++;
        if(NEXT_ROUTE === ROUTE_STACK.length) NEXT_ROUTE = 0;

        var next = ROUTE_STACK[NEXT_ROUTE];

        navigator.push({
            title: next.title,
            id: next.id
        });

    },
    LeftButton(route, navigator, index, navState) {
        return (
            <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}>
                <Text style={{color: 'white', margin: 10,}}>
                    {route.title}
                </Text>
            </TouchableOpacity>
        );
    },
    RightButton(route, navigator, index, navState) {
        return (
            <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}
                              onPress={() => this.Next(navigator)}>
                <View style={styles.dots}>
                    <View style={[styles.dot, styles.dot1]}></View>
                    <View style={[styles.dot, styles.dot2]}></View>
                    <View style={[styles.dot, styles.dot3]}></View>
                </View>
            </TouchableOpacity>
        );
    },
    Title(route, navigator, index, navState) {

    }

};

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    navigationBar: {
        backgroundColor: '#0082FF'
    },
    dots: {
        position: 'relative',
        height: 30,
        width: 30,
        padding: 5,
        marginRight: 20,
    },
    dot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#FFFFFF',
        position: 'absolute'
    },
    dot1: {
        right: 8
    },
    dot2: {
        bottom: 0
    },
    dot3: {
        bottom: 0,
        right: 0
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

module.exports = NavigationBar;
