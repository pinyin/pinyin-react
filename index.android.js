/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
//'use strict';

var React = require('react-native');

var {
    AppRegistry,
    StyleSheet,
    Component,
    Text,
    View,
    Navigator,
    TouchableOpacity,
    } = React;

//var PinYinDataOld = require('./PinYinData');
//console.log("old", PinYinDataOld);
var PinYinData = require('./data/data');

var SplashPage = require('./SplashPage');
var NoNavigatorPage = require('./NoNavigatorPage');

var ListInitials = require('./ListInitials');
var ListCombinations = require('./ListCombinations');
var ListFinals = require('./ListFinals');

var NoTransition = {
  opacity: {
    from: 1,
    to: 1,
    min: 1,
    max: 1,
    type: 'linear',
    extrapolate: false,
    round: 100,
  },
};

var ROUTE_STACK = [
  {id: 'ListCombinations', component: ListCombinations, index: 0, title:'Combinations', color: '#0082FF'},
  {id: 'ListInitials', component: ListInitials, index: 1, title:'Initials', color: '#689DE5'},
  {id: 'ListFinals', component: ListFinals, index: 2, title:'Finals', color: '#FF9B9E'}
];
var INIT_ROUTE_INDEX = 0;

// Custom animation
var NoTransition = {
  opacity: {
    from: 1,
    to: 1,
    min: 1,
    max: 1,
    type: 'linear',
    extrapolate: false,
    round: 100,
  },
};

// Create APP
class App extends Component {

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState(){
    return {
      color: {backgroundColor: '#0082FF'}
    }
  }

  render() {

    return (
        <Navigator
            initialRoute={{id: 'ListCombinations', name: 'Index'}}
            renderScene={this.renderScene.bind(this)}
            configureScene={(route) => {
              if (route.sceneConfig) {
                return route.sceneConfig;
              }
              transition = Navigator.SceneConfigs.FloatFromBottomAndroid
              transition.gestures = null
              return transition
              //return Navigator.SceneConfigs.PushFromRight;
            }}
        />
    );

  }

  renderScene(route, navigator) {
    var routeId = route.id;

    if (routeId === 'SplashPage') {
      return (
          <ListInitials
              navigator={navigator} pinyin={PinYinData} style={styles.list} />
      );
    }
    if (routeId === 'ListCombinations') {
      //return (
      //    <ListFinals
      //        navigator={navigator} pinyin={PinYinData} />
      //);
      return (
          <ListCombinations
              navigator={navigator} pinyin={PinYinData} style={styles.list} />
      );
    }
    if (routeId === 'ListInitials') {
      return (
          <ListInitials
              navigator={navigator} pinyin={PinYinData} style={styles.list} />
      );
    }
    if (routeId === 'ListFinals') {
      return (
          <ListFinals
              navigator={navigator} pinyin={PinYinData} />
      );
    }
    if (routeId === 'NoNavigatorPage') {
      return (
          <NoNavigatorPage
              navigator={navigator} />
      );
    }
    return this.noRoute(navigator);

  }
  noRoute(navigator) {
    return (
        <View style={{flex: 1, alignItems: 'stretch', justifyContent: 'center'}}>
          <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
                            onPress={() => navigator.pop()}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>请在 index.js 的 renderScene 中配置这个页面的路由</Text>
          </TouchableOpacity>
        </View>
    );
  }

}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('appPinYinReactNative', () => App);