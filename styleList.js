module.exports = require('react-native').StyleSheet.create(
    {
        navbar: {
            backgroundColor: '#0082FF'
        },
        dots: {
            position: 'relative',
            height: 30,
            width: 30,
            padding: 5,
            marginRight: 20,
        },
        dot: {
            width: 10,
            height: 10,
            borderRadius: 5,
            backgroundColor: '#FFFFFF',
            position: 'absolute'
        },
        dot1: {
            right: 8
        },
        dot2: {
            bottom: 0
        },
        dot3: {
            bottom: 0,
            right: 0
        },
        container: {
            flex: 1,
            backgroundColor: '#0082FF'
        },
        menuBg: {
            backgroundColor: 'red',
            position: 'absolute',
            height: 140,
            width: 200
        },
        activityIndicator: {
            alignItems: 'center',
            justifyContent: 'center',
        },
        header: {
            height: 60,
            flexDirection: 'column',
            paddingTop: 25
        },
        headerText: {
            fontWeight: 'bold',
            fontSize: 20,
            color: 'white'
        },
        intro: {
            padding: 20,
            paddingTop: 60,
            paddingBottom: 10
        },
        introTextTitle: {
            fontSize: 40,
            paddingBottom: 20,
            color: 'white',
            fontWeight: 'normal'
        },
        introText: {
            fontSize: 15,
            color: 'white',
            lineHeight: 24
        },
        text: {
            color: 'white',
            paddingHorizontal: 8,
            fontSize: 16
        },
        row: {
            flexDirection: 'row',
            paddingVertical: 10,
            paddingLeft: 16,
            paddingRight: 16
        },
        rowText: {},
        subText: {},
        section: {
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 6
        }
    }
);