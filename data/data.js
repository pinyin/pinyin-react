import CombinationsGenerated from './combinations';

// TODO: tone should be with number ex. 'a1', instead of 'a'.

var PinYinData = {
    "menu": [
        {"url": "/", label: "Home"},
        {"url": "chart", label: "Chart"},
        {"url": "initials", label: "Initials"},
        {"url": "finals", label: "Finals"}
    ],
    "home": {
        "title": "Pin Yin",
        "info": "Hanyu Pinyin or Pinyin is the Romanized Chinese Phonetic System. It can be used to teach and learn Chinese, or as an input method for typing Chinese characters.   "
    },
    "combinations": {
        "content": {
            "title": "PinYin chart",
            "info": "All possible combinations of intials and finals in pinyin system. Pay attention to the tonal mark and subtle sound differentiations."
        },
        list: CombinationsGenerated.combinations,
        listBlob: CombinationsGenerated.blob
    },
    "initials": {
        "content": {
            "title": "Initials",
            "info": "Initials are the consonants. It has its own pronouciations preference when it stands alone. Try some of them and listen to the sound carefully."
        },
        list: [
            [
                {
                    "combination": "bpmf",
                    "final": "o",
                    "tones": [
                        {
                            "tone": "a1",
                            "label": "a"
                        },
                        {
                            "tone": "a2",
                            "label": "a"
                        },
                        {
                            "tone": "a3",
                            "label": "a"
                        },
                        {
                            "tone": "fo1",
                            "label": "f"
                        }
                    ]
                },
                {
                    "combination": "dkt",
                    "final": "o",
                    "tones": [
                        {
                            "tone": "bo",
                            "label": "b"
                        },
                        {
                            "tone": "po",
                            "label": "p"
                        },
                        {
                            "tone": "mo",
                            "label": "m"
                        },
                        {
                            "tone": "fo",
                            "label": "f"
                        }
                    ]
                },
                {
                    "combination": "hehe",
                    "final": "o",
                    "tones": [
                        {
                            "tone": "bo",
                            "nr": 1,
                            "label": "b"
                        },
                        {
                            "tone": "po",
                            "nr": 1,
                            "label": "p"
                        },
                        {
                            "tone": "mo",
                            "nr": 1,
                            "label": "m"
                        },
                        {
                            "tone": "fo",
                            "nr": 1,
                            "label": "f"
                        }
                    ]
                }
            ]
        ]
    },
    "finals": {
        "content": {
            "title": "Finals",
            "info": "Finals consists of one, two, three or four vowls sometimes with consonant. The tonal sign is marked on top of it. The order is a e i o u v. "
        },
        list: [
            [
                {
                    "tones": [
                        {
                            "tone": "a1",
                            "label": "a"
                        },
                        {
                            "tone": "a2",
                            "label": "a"
                        },
                        {
                            "tone": "a3",
                            "label": "a"
                        },
                        {
                            "tone": "a4",
                            "label": "a"
                        }
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "o1",
                            "label": "o"
                        },
                        {
                            "tone": "o2",
                            "label": "o"
                        },
                        {
                            "tone": "o3",
                            "label": "o"
                        },
                        {
                            "tone": "o4",
                            "label": "o"
                        }
                    ]
                },
                {
                    "tones": [
                        {
                            "tone": "e",
                            "nr": 1,
                            "label": "e"
                        },
                        {
                            "tone": "e",
                            "nr": 2,
                            "label": "e"
                        },
                        {
                            "tone": "e",
                            "nr": 3,
                            "label": "e"
                        },
                        {
                            "tone": "e",
                            "nr": 4,
                            "label": "e"
                        }
                    ]
                }
            ]
        ]
    }
};

export default PinYinData;

const Combinations = PinYinData.combinations;
const Initials = PinYinData.initials;
const Finals = PinYinData.finals;
const Home = PinYinData.home;

export { Combinations, Initials, Finals, Home };