/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
var SoundButton = require('./SoundButton');
var StatusBarAndroid = require('react-native-android-statusbar');

var {
    StyleSheet,
    Text,
    View,
    Component,
    Navigator,
    ListView,
    TouchableOpacity,
    ActivityIndicatorIOS, // Should be ProgressBarAndroid
    } = React;

class PlayList extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        this.bindMethods();
    }

    bindMethods() {
        if (!this.bindableMethods) {
            return;
        }

        for (var methodName in this.bindableMethods) {
            this[methodName] = this.bindableMethods[methodName].bind(this);
        }
    }

    getInitialState() {

        var getSectionData = (dataBlob, sectionID) => {
            return dataBlob[sectionID];
        }

        var getRowData = (dataBlob, sectionID, rowID) => {
            return dataBlob[sectionID + ':' + rowID];
        }

        return {
            loaded: false,
            dataSource: new ListView.DataSource({
                getSectionData: getSectionData,
                getRowData: getRowData,
                rowHasChanged: (row1, row2) => row1 !== row2,
                sectionHeaderHasChanged: (s1, s2) => s1 !== s2
            })
        }


    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {

        this.setState({
            dataSource: this.state.dataSource.cloneWithRowsAndSections(this.props.pinyin.Combinations.listBlob.combinations.data, this.props.pinyin.Combinations.listBlob.combinations.sectionIDs, this.props.pinyin.Combinations.listBlob.combinations.rowIDs),
            loaded: true
        });

    }

    render() {

        StatusBarAndroid.setHexColor('#0082FF');

        if (!this.state.loaded) {
            return this.renderLoadingView();
        }

        return (
            <Navigator
                renderScene={this.renderScene.bind(this)}
                navigator={this.props.navigator}
                navigationBar={
                <Navigator.NavigationBar style={styles.navbar}
                                         routeMapper={NavigationBarRouteMapper} />
            } />
        )
    }

    renderScene(route, navigator) {
        return this.renderListView();
    }

    renderLoadingView() {
        return (
            <View style={styles.header}>
                <View style={styles.container}>
                    <ActivityIndicatorIOS
                        animating={!this.state.loaded}
                        style={[styles.activityIndicator, {height: 80}]}
                        size="large"
                    />
                </View>
            </View>
        );
    }

    renderListView() {
        return (
            <View style={styles.container}>
                <ListView
                    dataSource={this.state.dataSource}
                    style={styles.listview}
                    renderRow={this.renderRow}
                    renderHeader={() => <View style={styles.intro}>
                        <Text style={styles.introTextTitle} onPress={this.onPressTitle}>Combinations</Text>
                        <Text style={styles.introText}>Finals consists of one, two, three or four vowls sometimes with consonant. The tonal sign is marked on top of it. The order is a e i o u v.</Text>
                    </View>}
                    fastScrollEnabled={true}
                    fastScrollAlwaysVisible={true}
                />
            </View>
        );
    }

    renderSectionHeader(sectionData, sectionID) {
        return (
            <View style={styles.section}>
                <Text style={styles.text}>{sectionData}</Text>
            </View>
        );
    }
};

Object.assign(PlayList.prototype, {
    bindableMethods: {
        renderRow: function (rowData, sectionID, rowID) {

            return (
                <View style={styles.row}>
                    <SoundButton tone={rowData.tones[0]}></SoundButton>
                    <SoundButton tone={rowData.tones[1]}></SoundButton>
                    <SoundButton tone={rowData.tones[2]}></SoundButton>
                    <SoundButton tone={rowData.tones[3]}></SoundButton>
                </View>
            );

        }
    }
});

var NavigationBarRouteMapper = {

    LeftButton(route, navigator, index, navState) {
        return (
            <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}>
                <Text style={{color: 'white', margin: 10,}}>
                    PinYin
                </Text>
            </TouchableOpacity>
        );
    },
    RightButton(route, navigator, index, navState) {
        return (
            <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}
                              onPress={() => navigator.parentNavigator.push({id: 'ListFinals'})}>
                <View style={styles.dots}>
                    <View style={[styles.dot, styles.dot1]}></View>
                    <View style={[styles.dot, styles.dot2]}></View>
                    <View style={[styles.dot, styles.dot3]}></View>
                </View>
            </TouchableOpacity>
        );
    },
    Title(route, navigator, index, navState) {

    }

};

var styles = require('./styleList.js');

//AppRegistry.registerComponent('appPinYinReactNative', () => PlayList);
module.exports = PlayList;